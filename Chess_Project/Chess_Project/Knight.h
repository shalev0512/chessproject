#pragma once
#include "Piece.h"
#include <cmath>

#define KNIGHT_TYPE 'N'

class Knight : public Piece
{
public:
	Knight(const char side, int x, int y);
	Knight(const Knight& other);
	Piece* clone() const override;
	int legalMove(const int destX, const int destY, Piece* board[ROW][COL]) override;
};