#include "Rook.h"

Rook::Rook(const char side, int x, int y) :Piece(side, ROOK_TYPE, x, y)
{
}

Rook::Rook(const Rook& other) :Piece(other)
{
}

Piece* Rook::clone() const
{
	return new Rook(*this);
}

int Rook::legalMove(const int destX, const int destY, Piece* board[ROW][COL])
{
	// Checking if the dest is the same as the current piece's location on board
	if (this->_x == destX && this->_y == destY)
	{
		return ERR_7; // Dest the Src are the same (locations on board)
	}

	// Checking if the new dest is invalid - movement check
	if (!((this->_x == destX && this->_y != destY) || (this->_x != destX && this->_y == destY)))
	{
		return ERR_6; // This piece can't possibly go there from the current position
	}


	// The move is just a move
	return LEG_0;
}