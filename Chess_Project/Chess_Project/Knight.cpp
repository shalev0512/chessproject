#include "Knight.h"

Knight::Knight(const char side, int x, int y) :Piece(side, KNIGHT_TYPE, x, y)
{
}

Knight::Knight(const Knight& other) :Piece(other)
{
}

Piece* Knight::clone() const
{
	return new Knight(*this);
}

int Knight::legalMove(const int destX, const int destY, Piece* board[ROW][COL])
{
	// Checking if the dest is the same as the current piece's location on board
	if (this->_x == destX && this->_y == destY)
	{
		return ERR_7; // Dest the Src are the same (locations on board)
	}

	// Checking if the new dest is invalid - movement check
	if (!((destX == this->_x + 2 && destY == this->_y + 1) ||
		(destX == this->_x + 2 && destY == this->_y - 1) ||
		(destX == this->_x + 1 && destY == this->_y + 2) ||
		(destX == this->_x - 1 && destY == this->_y + 2) ||
		(destX == this->_x - 2 && destY == this->_y + 1) ||
		(destX == this->_x - 2 && destY == this->_y - 1) ||
		(destX == this->_x + 1 && destY == this->_y - 2) ||
		(destX == this->_x - 1 && destY == this->_y - 2)))
	{
		return ERR_6; // This piece can't possibly go there from the current position
	}

	// Checking if the dest has a piece of the current side playing
	if ((board[destY][destX]->getType() != '#' && board[destY][destX]->getSide() == this->_side))
	{
		return ERR_3; // Trying to capture a piece from the current size playing
	}

	// The move is just a move
	return LEG_0;
}
