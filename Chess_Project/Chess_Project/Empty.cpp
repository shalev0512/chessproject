#include "Empty.h"

Empty::Empty(const char side, int x, int y) :Piece(side, EMPTY_TYPE, x, y)
{

}

Empty::Empty(const Empty& other) :Piece(other)
{
}

Piece* Empty::clone() const
{
    return new Empty(*this);
}

int Empty::legalMove(const int destX, const int destY, Piece* board[ROW][COL])
{
    return 4;
}
