#include "Piece.h"
#include "Empty.h"
#include <iostream>

Piece::Piece(const char side, const char type, int x, int y) :_side(side), _x(x), _y(y)
{
	// Setting type upper or lower
	if (this->_side == WHITE)
	{
		this->_type = tolower(type);
	}
	else
	{
		this->_type = type;
	}
}

Piece::Piece(const Piece& other)
{
	this->_side = other.getSide();
	this->_type = other.getType();
	this->_x = other.getX();
	this->_y = other.getY();
}

char Piece::getType() const
{
	return this->_type;
}

char Piece::getX() const
{
	return this->_x;
}

char Piece::getY() const
{
	return this->_y;
}

char Piece::getSide() const
{
	return this->_side;
}

void Piece::setX(int x)
{
	this->_x = x;
}

void Piece::setY(int y)
{
	this->_y = y;
}

void Piece::move(const int destX, const int destY)
{
	this->_x = destX;
	this->_y = destY;
}
