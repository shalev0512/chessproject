#pragma once
#include "Piece.h"

#define QUEEN_TYPE 'Q'
#define QUEEN_X 4

class Queen : public Piece
{
public:
	Queen(const char side, int y);
	Queen(const Queen& other);
	Piece* clone() const override;
	int legalMove(const int destX, const int destY, Piece* board[ROW][COL]) override;
};