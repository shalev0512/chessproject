#include "King.h"

King::King(const char side, int y) :Piece(side, KING_TYPE, KING_X, y)
{
}

King::King(const King& other) :Piece(other)
{

}

Piece* King::clone() const
{
	return new King(*this);
}


int King::legalMove(const int destX, const int destY, Piece* board[ROW][COL])
{
	// Checking if the dest is the same as the current piece's location on board
	if (this->_x == destX && this->_y == destY)
	{
		return ERR_7; // Dest the Src are the same (locations on board)
	}

	// Checking if the new dest is invalid - movement check
	if (!((std::abs(destX - this->getX()) == 1 && std::abs(destY - this->getY()) == 1) ||
		(std::abs(destX - this->getX()) == 1 && std::abs(destY - this->getY()) == 0) ||
		(std::abs(destX - this->getX()) == 0 && std::abs(destY - this->getY()) == 1)))
	{
		return ERR_6; // This piece can't possibly go there from the current position
	}

	// Checking if the dest has a piece of the current side playing
	if (board[destY][destX]->getType() != '#' && board[destY][destX]->getSide() == this->_side)
	{
		return ERR_3; // Trying to capture a piece from the current size playing
	}

	// The move is just a move
	return LEG_0;
}

