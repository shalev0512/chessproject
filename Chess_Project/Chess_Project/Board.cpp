#include "Board.h"
#include <iostream>
Board::Board() :_turn(BLACK)
{
    //setting the board to a starting position:
    //Black Rooks
    this->_boardArr[0][0] = new Rook(BLACK, 0, 0);
    this->_boardArr[0][7] = new Rook(BLACK, 7, 0);
    //White Rooks
    this->_boardArr[7][0] = new Rook(WHITE, 0, 7);
    this->_boardArr[7][7] = new Rook(WHITE, 7, 7);

    //Black Knights
    this->_boardArr[0][1] = new Knight(BLACK, 1, 0);
    this->_boardArr[0][6] = new Knight(BLACK, 6, 0);
    //White Knights
    this->_boardArr[7][1] = new Knight(WHITE, 1, 7);
    this->_boardArr[7][6] = new Knight(WHITE, 6, 7);

    //Black Bishops
    this->_boardArr[0][2] = new Bishop(BLACK, 2, 0);
    this->_boardArr[0][5] = new Bishop(BLACK, 5, 0);
    //White Bishops
    this->_boardArr[7][2] = new Bishop(WHITE, 2, 7);
    this->_boardArr[7][5] = new Bishop(WHITE, 5, 7);

    //Black King
    this->_boardArr[0][3] = new King(BLACK, 0);
    //White King
    this->_boardArr[7][3] = new King(WHITE, 7);

    //Black Queen
    this->_boardArr[0][4] = new Queen(BLACK, 0);
    //White Queen
    this->_boardArr[7][4] = new Queen(WHITE, 7);

    //all the Pawns
    for (int i = 0; i < ROW; i++)
    {
        this->_boardArr[1][i] = new Pawn(BLACK, i, 1);
        this->_boardArr[6][i] = new Pawn(WHITE, i, 6);
    }

    //all the empty slots
    for (int row = 2; row < ROW - 2; row++)
    {
        for (int col = 0; col < COL; col++)
        {
            this->_boardArr[row][col] = new Empty(BLACK, col, row);
        }
    }

    //buliding board string:
    for (int row = 0; row < ROW; row++)
    {
        for (int col = 0; col < COL; col++)
        {
            this->_boardString += this->_boardArr[row][col]->getType();
        }
    }
}

Board::~Board()
{
    //clean memory
    for (int row = 0; row < ROW; row++)
    {
        for (int col = 0; col < COL; col++)
        {
            delete this->_boardArr[row][col];
        }
    }

    //clean board string
    this->_boardString = "";
}

void Board::updateBoardString()
{
    this->_boardString = "";
    for (int row = 0; row < ROW; row++)
    {
        for (int col = 0; col < COL; col++)
        {
            this->_boardString += this->_boardArr[row][col]->getType();
        }
    }
}

void Board::switchTurn()
{
    if (this->_turn == BLACK)
    {
        this->_turn = WHITE;
        return;
    }
    this->_turn = BLACK;
}

bool Board::somthingInTheWay(Piece& p, int destX, int destY)
{
    //checking what the type of piece
    switch (p.getType())
    {
    case 'Q':
        return queenWayNotClear(p, destX, destY);
        break;
    case 'q':
        return queenWayNotClear(p, destX, destY);
        break;
    case 'R':
        return rookWayNotClear(p, destX, destY);
        break;
    case 'r':
        return rookWayNotClear(p, destX, destY);
        break;
    case 'B':
        return bishopWayNotClear(p, destX, destY);
        break;
    case 'b':
        return bishopWayNotClear(p, destX, destY);
        break;

    default:
        return false;
    }
}

bool Board::queenWayNotClear(Piece& p, int destX, int destY)const
{
    //if it wants to move on y cordinate
    if (p.getX() == destX)
    {
        //if src y is smaller then dest
        if (p.getY() > destY)
        {
            for (int i = p.getY() - 1; i > destY; i--)
            {
                if (this->_boardArr[i][destX]->getType() != '#')
                {
                    return true;
                }
            }
        }
        else
        {
            for (int i = p.getY() + 1; i < destY; i++)
            {
                if (this->_boardArr[i][destX]->getType() != '#')
                {
                    return true;
                }
            }
        }
        return false;
    }
    //if it wants to move on x cordinate
    else if (p.getY() == destY)
    {
        //if src x is smaller then dest
        if (p.getX() > destX)
        {
            for (int i = p.getX() - 1; i > destX; i--)
            {
                if (this->_boardArr[destY][i]->getType() != '#')
                {
                    return true;
                }
            }
        }
        else
        {
            for (int i = p.getX() + 1; i < destX; i++)
            {
                if (this->_boardArr[destY][i]->getType() != '#')
                {
                    return true;
                }
            }
        }
        return false;
    }
    //if it wants to move Diagonal
    else
    {
        if (p.getX() > destX)
        {
            //-x-y
            if (p.getY() > destY)
            {
                for (int i = 1; i < p.getY() - destY; i++)
                {
                    if (this->_boardArr[p.getY() - i][p.getX() - i]->getType() != '#')
                    {
                        return true;
                    }
                }
            }
            //-x+y
            else
            {
                for (int i = 1; i < destY - p.getY(); i++)
                {
                    if (this->_boardArr[p.getY() + i][p.getX() - i]->getType() != '#')
                    {
                        return true;
                    }
                }
            }
        }
        //+x
        else
        {
            //+x-y
            if (p.getY() > destY)
            {
                for (int i = 1; i < p.getY() - destY; i++)
                {
                    if (this->_boardArr[p.getY() - i][p.getX() + i]->getType() != '#')
                    {
                        return true;
                    }
                }
            }
            //+x+y
            else
            {
                for (int i = 1; i < destY - p.getY(); i++)
                {
                    if (this->_boardArr[p.getY() + i][p.getX() + i]->getType() != '#')
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}

bool Board::rookWayNotClear(Piece& p, int destX, int destY) const
{
    //if it wants to move on y cordinate
    if (p.getX() == destX)
    {
        //if src y is smaller then dest
        if (p.getY() > destY)
        {
            for (int i = p.getY() - 1; i > destY; i--)
            {
                if (this->_boardArr[i][destX]->getType() != '#')
                {
                    return true;
                }
            }
        }
        else
        {
            for (int i = p.getY() + 1; i < destY; i++)
            {
                if (this->_boardArr[i][destX]->getType() != '#')
                {
                    return true;
                }
            }
        }
        return false;
    }
    //if it wants to move on x cordinate
    else if (p.getY() == destY)
    {
        //if src x is smaller then dest
        if (p.getX() > destX)
        {
            for (int i = p.getX() - 1; i > destX; i--)
            {
                if (this->_boardArr[destY][i]->getType() != '#')
                {
                    return true;
                }
            }
        }
        else
        {
            for (int i = p.getX() + 1; i < destX; i++)
            {
                if (this->_boardArr[destY][i]->getType() != '#')
                {
                    return true;
                }
            }
        }
        return false;
    }
}

bool Board::bishopWayNotClear(Piece& p, int destX, int destY) const
{
    if (p.getX() > destX)
    {
        //-x-y
        if (p.getY() > destY)
        {
            for (int i = 1; i < p.getY() - destY; i++)
            {
                if (this->_boardArr[p.getY() - i][p.getX() - i]->getType() != '#')
                {
                    return true;
                }
            }
        }
        //-x+y
        else
        {
            for (int i = 1; i < destY - p.getY(); i++)
            {
                if (this->_boardArr[p.getY() + i][p.getX() - i]->getType() != '#')
                {
                    return true;
                }
            }
        }
    }
    //+x
    else
    {
        //+x-y
        if (p.getY() > destY)
        {
            for (int i = 1; i < p.getY() - destY; i++)
            {
                if (this->_boardArr[p.getY() - i][p.getX() + i]->getType() != '#')
                {
                    return true;
                }
            }
        }
        //+x+y
        else
        {
            for (int i = 1; i < destY - p.getY(); i++)
            {
                if (this->_boardArr[p.getY() + i][p.getX() + i]->getType() != '#')
                {
                    return true;
                }
            }
        }
    }
    return false;
}

const Piece& Board::getKing(char side)
{
    for (int r = 0; r < ROW; r++)
    {
        for (int c = 0; c < COL; c++)
        {
            if (this->_boardArr[r][c]->getSide() == side && toupper(this->_boardArr[r][c]->getType()) == 'K')
            {
                return *(this->_boardArr[r][c]);
            }
        }
    }
    //if the king isn't found returning other piece
    if (toupper(this->_boardArr[0][0]->getType()) != 'K')
    {
        return *(this->_boardArr[0][0]);
    }
    //else
    return *(this->_boardArr[1][0]);
}

bool Board::isKingInCheckAfterMove(const int srcX, const int srcY, const int destX, const int destY, const Piece& king)
{
    //if there is no king
    if (toupper(king.getType()) != KING_TYPE)
    {
        return true;
    }

    //else
    bool isPinnedPiece = false;
    int kingY = king.getY();
    int kingX = king.getX();
    char myKingChar = king.getType();

    // Temperary change so the piece will not interapt with the calculations
    Piece& originalPiece = *(this->_boardArr[srcY][srcX]);
    Piece& copyKing = *(this->_boardArr[kingY][kingX]);
    Piece& copyDestPiece = *(this->_boardArr[destY][destX]);
    this->_boardArr[srcY][srcX] = new Empty(BLACK, srcX, srcY);
    this->_boardArr[destY][destX] = &originalPiece;

    if (toupper(originalPiece.getType()) == 'K' && originalPiece.getSide() == king.getSide())
    {
        kingY = destY;
        kingX = destX;
    }

    // Running through the board seeing if after the move the king will be in check
    for (int r = 0; r < ROW; ++r)
    {
        for (int c = 0; c < COL; c++)
        {
            // Saving the current piece and checking if its the opponent's piece
            Piece& currentPiece = *(this->_boardArr[r][c]);
            if (currentPiece.getSide() != king.getSide() && currentPiece.getType() != myKingChar)
            {
                // If legalMove returned 0
                if (currentPiece.legalMove(kingX, kingY, this->_boardArr) == 1 || currentPiece.legalMove(kingX, kingY, this->_boardArr) == 0)
                {
                    if (!somthingInTheWay(currentPiece, kingX, kingY))
                    {
                        isPinnedPiece = true;
                    }
                }
            }
        }
    }

    this->_boardArr[kingY][kingX] = &copyKing;
    // Switching the pieces back
    //delete this->_boardArr[destY][destX];
    this->_boardArr[destY][destX] = &copyDestPiece;

    this->_boardArr[srcY][srcX] = &originalPiece;


    return isPinnedPiece;
}

const std::string& Board::getBoardString() const
{
    return this->_boardString;
}

std::string Board::updateBackend(const std::string& update)
{
    //checking that update is valid
    if (update.size() != 4)
    {
        if (update != "e")
            std::cerr << "Move str is ilegall!!" << std::endl;
        return std::to_string(ERR_9);
    }
    //getting from where the move is to where
    int fromX = update[0] - 'a';
    int fromY = update[1] - '0' - 1;
    int toX = update[2] - 'a';
    int toY = update[3] - '0' - 1;
    std::string code = "";
    //from side
    char fromSide = ' ';

    //is in chess
    bool isPinned = false;

    //Index error
    if (!(toX >= 0 && toX < COL && toY >= 0 && toY < ROW))
    {
        return std::to_string(ERR_5);
    }
    //same slot for src and dest
    else if (fromX == toX && fromY == toY)
    {
        return std::to_string(ERR_7);
    }
    //No actual piece in the from slot
    else if (this->_boardArr[fromY][fromX]->getType() == '#' || this->_boardArr[fromY][fromX]->getSide() != this->_turn)
    {
        return std::to_string(ERR_2);
    }
    //if the dest slot has a piece the same color as the from
    else if ((this->_boardArr[fromY][fromX]->getSide() == this->_boardArr[toY][toX]->getSide()) && this->_boardArr[toY][toX]->getType() != '#')
    {
        return std::to_string(ERR_3);
    }

    //somthing in the way of movmenent
    if (somthingInTheWay(*(this->_boardArr[fromY][fromX]), toX, toY))
    {
        return std::to_string(ERR_6);
    }


    fromSide = this->_boardArr[fromY][fromX]->getSide();

    const Piece* whiteKingPiece = &(getKing(WHITE));
    const Piece* blackKingPiece = &(getKing(BLACK));

    // Checking if Pinned
    if (this->_turn == WHITE)
    {
        if (isKingInCheckAfterMove(fromX, fromY, toX, toY, *(whiteKingPiece)))
        {
            return std::to_string(ERR_4);
        }
    }
    else if (isKingInCheckAfterMove(fromX, fromY, toX, toY, *(blackKingPiece)))
    {
        return std::to_string(ERR_4);
    }

    // checking if the move is legal
    code = std::to_string(this->_boardArr[fromY][fromX]->legalMove(toX, toY, this->_boardArr));

    if (code == std::to_string(LEG_0))
    {
        // Checking if the move creates a check
        if (this->_turn != WHITE)
        {
            if (isKingInCheckAfterMove(fromX, fromY, toX, toY, *(whiteKingPiece)))
            {
                code = std::to_string(LEG_1);
            }
        }
        else if (isKingInCheckAfterMove(fromX, fromY, toX, toY, *(blackKingPiece)))
        {
            code = std::to_string(LEG_1);
        }
    }

    //if the move is legal
    if (code == std::to_string(LEG_0) || code == std::to_string(LEG_1) || code == std::to_string(LEG_8))
    {
        //deleting the slot we move the piece into
        delete this->_boardArr[toY][toX];

        //putting a copy of the piece were moving
        this->_boardArr[toY][toX] = this->_boardArr[fromY][fromX];

        //uptading x and y
        this->_boardArr[toY][toX]->setX(toX);
        this->_boardArr[toY][toX]->setY(toY);

        //making the from an empty slot
        this->_boardArr[fromY][fromX] = new Empty(BLACK, fromX, fromY);

        //updating the new board string
        updateBoardString();

        //switching turn 
        switchTurn();
    }
    //returning the code to frontend:
    return code;

}

const char Board::getTurn() const
{
    return this->_turn;
}
