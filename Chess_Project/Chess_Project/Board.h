#pragma once
#include <string>
#include "Piece.h"
#include "Pipe.h"
#include "Piece.h"
#include "Bishop.h"
#include "Empty.h"
#include "King.h"
#include "Knight.h"
#include "Pawn.h"
#include "Queen.h"
#include "Rook.h"

#define ROW 8
#define COL 8

class Board
{
private:
	Piece* _boardArr[ROW][COL];
	char _turn;
	std::string _boardString;

public:
	Board();
	~Board();
	void updateBoardString();
	void switchTurn();
	bool somthingInTheWay(Piece& p, int destX, int destY);

	bool queenWayNotClear(Piece& p, int destX, int destY)const;
	bool rookWayNotClear(Piece& p, int destX, int destY)const;
	bool bishopWayNotClear(Piece& p, int destX, int destY)const;

	//check & checkmate methods
	const Piece& getKing(char side);
	bool isKingInCheckAfterMove(const int srcX, const int srcY, const int destX, const int destY, const Piece& king);
	bool checkMateAfterThisMove(const int srcX, const int srcY, const int destX, const int destY, const Piece& opKing);

	//build board string
	const std::string& getBoardString() const;
	std::string updateBackend(const std::string& update);

	//getters
	const char getTurn() const;
};
