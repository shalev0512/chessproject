#pragma once
#include "Piece.h"

#define BISHOP_TYPE 'B'
class Bishop : public Piece
{
public:
	Bishop(const char side, int x, int y);
	Bishop(const Bishop& other);
	Piece* clone() const override;
	int legalMove(const int destX, const int destY, Piece* board[ROW][COL]) override;
};