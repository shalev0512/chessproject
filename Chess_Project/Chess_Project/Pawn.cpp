#include "Pawn.h"

Pawn::Pawn(char side, int x, int y) :Piece(side, PAWN_TYPE, x, y)
{
}

Pawn::Pawn(const Pawn& other) :Piece(other)
{
}

Piece* Pawn::clone() const
{
    return new Pawn(*this);
}

int Pawn::legalMove(const int destX, const int destY, Piece* board[ROW][COL])
{
    //if it's his first move
    if (((this->getY() == 1 && destY == 3 && this->getSide() == BLACK) || (this->getY() == 6 && destY == 4 && this->getSide() == WHITE)) && board[destY][destX]->getType() == TYPE_EMPTY)
    {
        return LEG_0;
    }
    //checking that he ain't trying to move backwords
    if ((this->getSide() == BLACK && destY < this->getY()) || (this->getSide() == WHITE && destY > this->getY()))
    {
        return ERR_6;
    }

    // Check for forward movement
    if (this->getX() == destX)
    {
        int steps = std::abs(this->getY() - destY);
        if ((steps > 2 && ((this->getX() == 1 && this->getSide() == BLACK) || (this->getX() == 6 && this->getSide() == WHITE))) ||
            (steps > 1 && ((this->getX() != 1 && this->getSide() == BLACK) || (this->getX() != 6 && this->getSide() == WHITE)))) {
            return ERR_6; // Invalid number of steps for pawn's movement
        }
        // Check if the destination is blocked by another piece
        if (board[destY][destX]->getType() != TYPE_EMPTY) {
            return ERR_6; // Destination is not empty, blocking the pawn's move
        }
        return LEG_0; // Forward move is legal
    }
    // Check for diagonal movement (capture)
    else
    {
        if (!((std::abs(this->getX() - destX) == 1) && (std::abs(this->getY() - destY) == 1))) {
            return ERR_6; // Invalid diagonal move
        }
        // Check if the destination is a valid capture (opponent's piece)
        if (board[destY][destX]->getType() == TYPE_EMPTY) {
            return ERR_6; // No opponent's piece to capture
        }
        return LEG_0; // Diagonal move is legal
    }
}