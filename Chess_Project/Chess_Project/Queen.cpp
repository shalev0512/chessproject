#include "Queen.h"
#include <iostream>

Queen::Queen(const char side, int y) :Piece(side, QUEEN_TYPE, QUEEN_X, y)
{
}

Queen::Queen(const Queen& other) :Piece(other)
{
}

Piece* Queen::clone() const
{
	return new Queen(*this);
}

int Queen::legalMove(const int destX, const int destY, Piece* board[ROW][COL])
{
	// Calculate the absolute differences between coordinates
	int xDiff = std::abs(destX - this->getX());
	int yDiff = std::abs(destY - this->getY());

	// Checking if the dest is the same as the current piece's location on board
	if (this->_x == destX && this->_y == destY)
	{
		return ERR_7; // Dest the Src are the same (locations on board)
	}

	// Checking if the new dest is invalid - movement check
	if (!((this->_x == destX && this->_y != destY) || (this->_x != destX && this->_y == destY)) &&
		xDiff != yDiff)
	{
		return ERR_6; // This piece can't possibly go there from the current position
	}


	return LEG_0;
}