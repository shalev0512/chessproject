#include "Bishop.h"

Bishop::Bishop(const char side, int x, int y) :Piece(side, BISHOP_TYPE, x, y)
{

}

Bishop::Bishop(const Bishop& other) : Piece(other)
{
}

Piece* Bishop::clone() const
{
    return new Bishop(*this);
}

int Bishop::legalMove(const int destX, const int destY, Piece* board[ROW][COL])
{
    // Calculate the absolute differences between coordinates
    int xDiff = std::abs(destX - this->getX());
    int yDiff = std::abs(destY - this->getY());

    // Check if the movement is diagonal (same difference in x and y)
    if (xDiff == yDiff)
    {
        return LEG_0; // Legal diagonal move
    }
    else
    {
        return ERR_6; // Not a diagonal move
    }
}
