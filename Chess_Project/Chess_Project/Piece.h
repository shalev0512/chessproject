#pragma once
#include <string>

#define BLACK 'B'
#define WHITE 'W'
#define ROW 8
#define COL 8
#define TYPE_EMPTY '#'

#define LEG_0 0
#define LEG_1 1
#define ERR_2 2
#define ERR_3 3
#define ERR_4 4
#define ERR_5 5
#define ERR_6 6
#define ERR_7 7
#define LEG_8 8


//if move is not a string with 4 chars.
#define ERR_9 9


class Piece
{
protected:
	int _x;
	int _y;
	char _side;
	char _type;

public:
	Piece(const char side, const char type, int x, int y);
	Piece(const Piece& other);

	virtual ~Piece() {}
	// Virtual function for cloning the Piece
	virtual Piece* clone() const = 0;

	virtual int legalMove(const int destX, const int destY, Piece* board[ROW][COL]) = 0;
	void move(const int destX, const int destY);

	//getters
	char getType() const;
	char getX() const;
	char getY() const;
	char getSide() const;

	//setters
	void setX(int x);
	void setY(int y);

};
