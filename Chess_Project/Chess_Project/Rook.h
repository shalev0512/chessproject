#pragma once
#include "Piece.h"

#define ROOK_TYPE 'R'

class Rook : public Piece
{
public:
	Rook(const char side, int x, int y);
	Rook(const Rook& other);
	Piece* clone() const override;
	int legalMove(const int destX, const int destY, Piece* board[ROW][COL]) override;
};
