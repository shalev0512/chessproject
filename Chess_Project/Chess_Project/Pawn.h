#pragma once
#include "Piece.h"

#define PAWN_TYPE 'P'
#define START_ROW_1 1
#define START_ROW_6 6

class Pawn : public Piece
{
public:
	Pawn(char side, int x, int y);
	Pawn(const Pawn& other);
	Piece* clone() const override;
	int legalMove(const int destX, const int destY, Piece* board[ROW][COL]) override;
};