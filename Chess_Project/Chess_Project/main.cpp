#include <iostream>
#include <string.h>
#include "Board.h"
#include "Pipe.h";


#define ROW 8
#define COL 8

void printBoard(std::string boardStr);
std::string sendAndGetMsg(Pipe& p, std::string MsgB);

int main()
{
	//vars
	Board* myBoard = new Board();
	std::string resultCode = "";
	std::string move = "";
	bool end_game = false;
	std::string msgBackend = myBoard->getBoardString() + "1";

	//creating a pipe
	Pipe frontendPipe;

	//if the conection worked
	if (frontendPipe.connect())
	{
		//printing the board
		printBoard(myBoard->getBoardString());
		std::cout << "it's " << myBoard->getTurn() << " turn" << std::endl;

		//sending the board string to front and getting the first move.
		frontendPipe.sendMessageToGraphics(&msgBackend[0]);
		move = frontendPipe.getMessageFromGraphics();
		while (resultCode != "9")
		{
			//update the backend with the move
			resultCode = myBoard->updateBackend(move);
			std::cout << "Result code is: " << resultCode << std::endl;

			//sending result code to frontend
			move = sendAndGetMsg(frontendPipe, resultCode);
		}

	}
	//if the connection faild:
	else
	{
		std::cerr << "Pipe connection faild";
		return -1;
	}
	return 0;
}

void printBoard(std::string boardStr)
{
	std::cout << " ";
	for (int i = 0; i < COL; i++)
	{
		std::cout << " " << char(i + 'a');
	}
	std::cout << std::endl;

	for (int i = 0; i < ROW; i++)
	{
		std::cout << std::to_string(i + 1) << " ";
		for (int j = 0; j < COL; j++)
		{
			std::cout << boardStr[i * COL + j] << " ";
		}
		std::cout << std::endl;
	}
}

std::string sendAndGetMsg(Pipe& p, std::string MsgB)
{
	//making the msg
	char* msgBackend = &MsgB[0];

	//sending the msg
	p.sendMessageToGraphics(msgBackend);

	//getting the respose
	return p.getMessageFromGraphics();
}