#pragma once
#include "Piece.h"
#define KING_TYPE 'K'
#define KING_X 3

class King : public Piece
{
public:
	King(const char side, int y);
	King(const King& other);
	Piece* clone() const override;
	int legalMove(const int destX, const int destY, Piece* board[ROW][COL]) override;
private:
	bool inCheck();
};
