#pragma once
#include "Piece.h"

#define EMPTY_TYPE '#'

class Empty : public Piece
{
public:
	Empty(const char side, int x, int y);
	Empty(const Empty& other);
	Piece* clone() const override;
	int legalMove(const int destX, const int destY, Piece* board[ROW][COL]) override;
};